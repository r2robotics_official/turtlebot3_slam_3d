1. bag-файл: https://yadi.sk/d/25QIovFwdHaSHw

2. rtabmap: https://github.com/introlab/rtabmap_ros

3. turtlebot3:

    cd ~/catkin_ws/src
    
    git clone https://github.com/ROBOTIS-GIT/turtlebot3_msgs.git
    
    git clone https://github.com/ROBOTIS-GIT/turtlebot3.git
    
    cd ~/catkin_ws && catkin_make

3. этот пакет:

    cd ~/catkin_ws/src

    git clone https://gitlab.com/r2robotics_official/turtlebot3_slam_3d.git
    
    cd ~/catkin_ws && catkin_make

--------------------------------------------------------------

**Запуск bag-файла:**

  roslaunch turtlebot3_slam_3d demo_bag.launch bag_file:=/absolute/path/to/bag_file.bag